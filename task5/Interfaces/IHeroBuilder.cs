﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using task5.Models;

namespace task5.Interfaces
{
	public interface IHeroBuilder
	{
		IHeroBuilder SetName(string name);
		IHeroBuilder SetHeight(string height);
		IHeroBuilder SetBuild(string build);
		IHeroBuilder SetHairColor(string hairColor);
		IHeroBuilder SetEyeColor(string eyeColor);
		IHeroBuilder SetClothing(string clothing);
		IHeroBuilder SetType(string type);
		IHeroBuilder AddToInventory(string item);
		Hero Build();
	}
}
