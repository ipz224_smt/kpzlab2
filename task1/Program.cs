﻿using task1.Interfaces;
using task1.Models;

Console.WriteLine("Buying from web site:\n");
ISubscriptionCreator subscriptionCreator = new WebSite();
ISubscription premiumSubscription = subscriptionCreator.CreateSubscription("premium");
premiumSubscription.DisplayInfo();

ISubscription educationalSubscription = subscriptionCreator.CreateSubscription("educational");
educationalSubscription.DisplayInfo();

ISubscription domesticSubscription = subscriptionCreator.CreateSubscription("domestic");
domesticSubscription.DisplayInfo();
