﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using task1.Interfaces;
using task1.Utils;

namespace task1.Models
{
	public class ManagerCall : ISubscriptionCreator
	{
		public ISubscription CreateSubscription(string subscriptionType)
		{
			ISubscription subscription = SubscriptionFactory.GetSubscription(subscriptionType);
			return subscription;
		}
	}
}
