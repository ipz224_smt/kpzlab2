﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task4.Models
{
	public class Virus(double weight, int age, string name, string type) : ICloneable
	{
		public double Weight { get; set; } = weight;
		public int Age { get; set; } = age;
		public string Name { get; set; } = name;
		public string Type { get; set; } = type;
		public List<Virus> Children { get; set; } = [];

		public object Clone()
		{
			Virus clone = new(Weight, Age, Name, Type);
			foreach (Virus child in Children)
			{
				clone.Children.Add((Virus)child.Clone());
			}
			return clone;
		}

		public void DisplayInfo()
		{
			Console.WriteLine($"Name: {Name}, Type: {Type}, Weight: {Weight}, Age: {Age}");
			foreach (Virus child in Children)
			{
				child.DisplayInfo();
			}
		}
	}

}
