﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using task2.Interfaces;

namespace task2.Models
{
	public class Netbook : IDevice
	{
		public string? Brand { get; set; }
		public string? Model { get; set; }

		public void DisplayInfo()
		{
			Console.WriteLine($"Netbook: {Brand} {Model}");
		}
	}
}
