﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using task2.Interfaces;

namespace task2.Models
{
	public class NetbookFactory : IDeviceFactory
	{
		public IDevice CreateDevice(string brand, string model)
		{
			return new Netbook { Brand = brand, Model = model };
		}
	}
}
